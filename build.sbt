name := "nref-slackbot"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies += "org.mockito" % "mockito-core" % "2.25.1" % Test
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.7" % "test"

libraryDependencies += "org.typelevel" %% "cats-core" % "1.6.0"
libraryDependencies += "org.typelevel" %% "cats-effect" % "1.2.0"

libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.4.1"
libraryDependencies += "org.json4s" %% "json4s-native" % "3.6.5"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.5"
libraryDependencies += "com.github.slack-scala-client" %% "slack-scala-client" % "0.2.4"

libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.10.2"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"

scalacOptions += "-Ypartial-unification"

addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.0-M4")

assemblyJarName in assembly := "slackBot.jar"