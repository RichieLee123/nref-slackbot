package core

import org.slf4j.{Logger, LoggerFactory}

trait LazyLogging {
  implicit protected lazy val log: Logger = LoggerFactory.getLogger(getClass.getName)
}
