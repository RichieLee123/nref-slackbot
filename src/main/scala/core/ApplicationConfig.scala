package core

import pureconfig.generic.auto._

case class ApplicationConfig(application: Application)

case class Application(slackApiKey: String,
                       nasaApiKey: String,
                       nasaApiUrl: String)

trait Config {
  implicit val appConfig: ApplicationConfig = pureconfig.loadConfigOrThrow[ApplicationConfig]
}
