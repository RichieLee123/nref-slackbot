import core.{Config, LazyLogging}
import messaging.MessageHandler
import akka.actor.ActorSystem
import commands.Command
import commands.impl.{ApodCommand, DefaultCommand}
import http.HttpRequester
import slack.api.SlackApiClient
import slack.rtm.SlackRtmClient

object ExitCodes {
  val Success = 0
  val ConfigLoadFailure = 1
}

object Main extends App with Config with LazyLogging {
  log.info("Application Starting")

  implicit val token: String = appConfig.application.slackApiKey
  implicit val system: ActorSystem = ActorSystem("slack")
  implicit val rtmClient: SlackRtmClient = SlackRtmClient(token)
  implicit val apiClient: SlackApiClient = SlackApiClient(token)

  val httpClient = new HttpRequester()

  implicit val defaultCommand: DefaultCommand = new DefaultCommand()
  implicit val commands: List[Command] = List(
    new ApodCommand(httpClient)
  )

  commands.foreach(c => log.info(s"command loaded: ${c.triggerMessage}"))

  rtmClient.onMessage(MessageHandler.handle(_))

  Runtime.getRuntime.addShutdownHook(new Thread(() => {
    log.info("Shutdown triggered. Application Exiting")
  }))
}
