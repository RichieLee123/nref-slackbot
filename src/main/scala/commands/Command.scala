package commands

import core.LazyLogging
import messaging.ParsedMessage
import slack.models.Message

import scala.util.Try

trait Command extends LazyLogging {
  def triggerMessage: String
  def execute(parsedMessage: ParsedMessage, message: Message): Try[Unit]
}