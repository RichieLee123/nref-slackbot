package commands

case class CommandError (message: String,
                         private val cause: Throwable = None.orNull)
  extends Exception(message, cause)
