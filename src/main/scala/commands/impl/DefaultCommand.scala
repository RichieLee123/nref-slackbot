package commands.impl

import messaging.ParsedMessage
import akka.actor.ActorSystem
import commands.Command
import slack.models.Message
import slack.rtm.SlackRtmClient

import scala.util.Try

class DefaultCommand(implicit rtmClient: SlackRtmClient,
                     actorSystem: ActorSystem) extends Command {
  override def triggerMessage: String = "default"

  override def execute(parsedMessage: ParsedMessage, message: Message): Try[Unit] =Try {
    val commandNotFoundString = s"Cannot find command for ${parsedMessage.command}"
    rtmClient.sendMessage(message.channel, commandNotFoundString)
  }
}
