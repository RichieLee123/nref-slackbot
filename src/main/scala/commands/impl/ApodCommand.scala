package commands.impl

import core.ApplicationConfig
import messaging.ParsedMessage
import akka.actor.ActorSystem

import commands.Command
import http.{HttpRequester, Method}
import slack.models.Message
import slack.rtm.SlackRtmClient
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scalaj.http.HttpResponse

import scala.concurrent.Future
import scala.util.Try

case class Apod(title: String, explanation: String, url: String)

class ApodCommand(httpClient: HttpRequester)(implicit rtmClient: SlackRtmClient,
                                             actorSystem: ActorSystem,
                                             config: ApplicationConfig) extends Command {
  override def triggerMessage: String = "apod"

  override def execute(parsedMessage: ParsedMessage, message: Message): Try[Unit] = Try {
    if (parsedMessage.params.isEmpty) {
      postApod(message.channel)
    } else {
      processParams(parsedMessage.params, message.channel)
    }
  }

  private def parseApodResponse(res: HttpResponse[String]): String = {
    implicit val formats = DefaultFormats
    val apod = parse(res.body).extract[Apod]
    s"""*${apod.title}*
       |
       |${apod.explanation}
       |${apod.url}""".stripMargin
  }

  private def postApod(channel: String): Future[Long] = {
    val response = httpClient.request(
      s"${config.application.nasaApiUrl}/planetary/apod?api_key=${config.application.nasaApiKey}",
      Method.GET,
      Map.empty)

    response.code match {
      case 200 => rtmClient.sendMessage(channel, parseApodResponse(response), None)
      case code@_ => rtmClient.sendMessage(channel, s"error contacting APOD api. ResponseCode $code", None)
    }
  }

  private def processParams(params: Map[String, String], channel: String) = {
    if (params contains "command") {
      params("command") match {
        case "help" => rtmClient.sendMessage(channel,"Displays the Nasa astrology Picture of the day in the channel", None)
        case wrong@_ => rtmClient.sendMessage(channel,s"parameters not recognised $wrong", None)
      }
    }
  }
}
