package http

import http.Method.method
import scalaj.http.Http
import scalaj.http.HttpResponse

object Method extends Enumeration {
  type method = Value
  val GET, POST, PUT, POP, PURGE = Value
}

class HttpRequester {
  def request(url: String, method: method, headers: Map[String,String]): HttpResponse[String] = Http(url)
      .headers("some" -> "")
      .method(method.toString)
      .headers(headers)
      .execute()
}
