package messaging

import core.LazyLogging

import scala.util.Try

case class ParsedMessage(command: String, params: Map[String, String])

object MessageParser extends LazyLogging {
  val commandRegex = "\\!(.\\w*)"r
  val paramRegex = """--(.\w*)=(".\w*")""".r

  private def extractParams(message: String): Map[String,String] = {

    val params = paramRegex.findAllIn(message).toList

    def extractParam(param: String): Option[(String, String)] = {
      val keyRegex = """--(.\w*)""".r
      val valueRegex = """=(".\w*")""".r

      val stripKey = (key: String) => key.replace("--", "").replace("\"", "")
      val stripValue = (value: String) => value.replace("=", "").replace("\"", "")

      for {
        k <- keyRegex.findFirstIn(param).map(stripKey(_))
        v <- valueRegex.findFirstIn(param).map(stripValue(_))
      } yield (k,v)
    }

    params.flatMap(extractParam).toMap
  }

  def parseMessage(message: String): Try[ParsedMessage] = Try {
      //at this point we will already know the message will start with a command so we can call get safely
      val command = commandRegex.findFirstIn(message).map(_.replace("!", "")).get
      log.info(s"parsing message for command $command")
      ParsedMessage(command, extractParams(message))
    }
}
