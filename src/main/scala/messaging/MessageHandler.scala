package messaging

import commands.Command
import core.LazyLogging
import slack.models.Message

object MessageHandler extends LazyLogging {
  def handle(message: Message)(implicit commands: List[Command], defaultCommand: Command): Unit = {
    val findCommandFor = (m: ParsedMessage) =>
      commands.find(c => c.triggerMessage == m.command) match {
        case Some(command) =>
          log.info(s"command found for message: $message - ${command.triggerMessage}")
          command
        case None =>
          log.info(s"no command found for message: $message")
          defaultCommand
      }

    if (message.text.startsWith("!"))
      MessageParser.parseMessage(message.text).map(p => findCommandFor(p).execute(p, message))
  }
}