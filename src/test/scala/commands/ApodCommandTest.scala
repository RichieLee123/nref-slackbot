package commands

import akka.actor.ActorSystem
import core.{Application, ApplicationConfig}
import commands.impl.ApodCommand
import org.scalatest.FlatSpec
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import slack.api.SlackApiClient
import slack.rtm.SlackRtmClient
import http.{HttpRequester, Method}
import messaging.ParsedMessage
import org.mockito.ArgumentMatchers.{any, eq => equ}
import org.mockito.stubbing.OngoingStubbing
import scalaj.http.HttpResponse
import slack.models.Message

import scala.io.Source

trait ApodTest extends MockitoSugar {
  implicit val config = mock[ApplicationConfig]
  implicit val system = mock[ActorSystem]
  implicit val rtmClient = mock[SlackRtmClient]
  implicit val apiClient = mock[SlackApiClient]

  val mockHttp = mock[HttpRequester]
  val apodCommand = new ApodCommand(mockHttp)

  def mockApplicationConfig(value: Application): OngoingStubbing[Application] = when(config.application).thenReturn(value)
  def validCallToNasaApiReturns(httpResponse: HttpResponse[String]): OngoingStubbing[HttpResponse[String]] =
    when(mockHttp.request(
        equ("https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY "),
        any[http.Method.Value],
        any[Map[String, String]]))
      .thenReturn(httpResponse)
}


class ApodCommandTest extends FlatSpec with MockitoSugar {
  behavior of "ApodCommand"

  it should "Describe what it does when it is given the help parameter" in new ApodTest {
    val msg = Message(
      ts = "some-timestamp",
      channel = "myChannel",
      user = "someUser",
      text = "!APOD --command=\"help\"",
      is_starred = Some(false),
      thread_ts = None)

    val parsedMessage = ParsedMessage("APOD", Map("command" -> "help"))

    apodCommand.execute(parsedMessage, msg)

    verify(rtmClient, times(1)).sendMessage(
      equ("myChannel"),
      equ("Displays the Nasa astrology Picture of the day in the channel"),
      equ(None))
  }

  it should "should make a request to the NASA api" in new ApodTest {
    mockApplicationConfig(Application("","myNasaApiKey", "https://api.nasa.gov"))

    val msg = Message(
      ts = "some-timestamp",
      channel = "myChannel",
      user = "someUser",
      text = "!APOD",
      is_starred = Some(false),
      thread_ts = None)

    val parsedMessage = ParsedMessage("APOD", Map.empty)
    apodCommand.execute(parsedMessage, msg)

    verify(mockHttp, times(1))
      .request("https://api.nasa.gov/planetary/apod?api_key=myNasaApiKey", Method.GET, Map.empty)
  }

  it should "post the picture to the channel" in new ApodTest {
    val validResponse = Source.fromResource("./exampleApodResponse.json").mkString
    mockApplicationConfig(Application("","myNasaApiKey", "https://api.nasa.gov"))

    val fakeResponse = HttpResponse[String](validResponse, 200, Map.empty)

    val messageText =
      """*Martian Moon Phobos Crosses the Sun*
        |
        |What's that passing in front of the Sun? It looks like a moon, but it can't be Earth's Moon, because it isn't round. It's the Martian moon Phobos.  The featured video was taken from the surface of Mars late last month by the Curiosity rover. Phobos, at 11.5 kilometers across, is 150 times smaller than Luna (our moon) in diameter, but also 50 times closer to its parent planet.  In fact, Phobos is so close to Mars that it is expected to break up and crash into Mars within the next 50 million years.  In the near term, the low orbit of Phobos results in more rapid solar eclipses than seen from Earth. The featured video has been sped up -- the actual transit took about 35 seconds. A similar video was taken of Mars' smaller and most distant moon Diemos transiting the Sun.  The videographer -- the robotic rover Curiosity -- continues to explore Gale crater, most recently an area with stunning vistas and unusual rocks dubbed Glen Torridon.    Retrospective: Previous APODs that appeared on April 10.
        |https://www.youtube.com/embed/Ce21mE1nXaU?rel=0""".stripMargin

    val msg = Message(
      ts = "some-timestamp",
      channel = "myChannel",
      user = "someUser",
      text = "!APOD",
      is_starred = Some(false),
      thread_ts = None)

    val parsedMessage = ParsedMessage("APOD", Map.empty)

    when(mockHttp.request(
      equ("https://api.nasa.gov/planetary/apod?api_key=myNasaApiKey"),
      any[http.Method.Value],
      any[Map[String, String]])).thenReturn(fakeResponse)

    apodCommand.execute(parsedMessage, msg)

    verify(rtmClient, times(1))
      .sendMessage(equ("myChannel"), equ(messageText), equ(None))
  }
}

