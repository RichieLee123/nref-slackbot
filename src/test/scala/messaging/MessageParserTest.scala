package messaging

import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Success}

class MessageParserTest extends FlatSpec with Matchers {
  behavior of "MessageParser"

  def successfulResult(command: String, params: Map[String, String]) = Success(ParsedMessage(command, params))

  it should "Parse a valid message with params" in {
    val message = s"""!command --with="some" --params="foo""""

    MessageParser.parseMessage(message) shouldBe
     successfulResult("command", Map("with" -> "some", "params" -> "foo"))
  }

  it should "Parse a value without params" in {
    val message = s"""!jumpDaFuckUp"""
    MessageParser.parseMessage(message) shouldBe successfulResult("jumpDaFuckUp", Map.empty)
  }

  it should "return not return params if the params are missing values" in {
    val message = s"""!command --with="""
    MessageParser.parseMessage(message) shouldBe successfulResult("command", Map.empty)
  }

  it should "not return params if they cannot be parsed" in {
    val message = "!command hello dave its a nice day isn't it"
    MessageParser.parseMessage(message) shouldBe successfulResult("command", Map.empty)
  }
}
