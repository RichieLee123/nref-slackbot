package messaging

import commands._
import org.mockito.ArgumentMatchers
import org.scalatest.{FlatSpec, Matchers}
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import slack.models.Message

class HandlerTest extends FlatSpec with Matchers with MockitoSugar {
  behavior of "MessageHandler"

  def any[T] = ArgumentMatchers.any[T]

  def mockTriggerMessage(command: Command, message: String) =
    when(command.triggerMessage).thenReturn(message)

  def verifyCommandNotExecuted(command: Command) =
    verify(command, times(0))
      .execute(any[ParsedMessage], any[Message])

  it should "call the correct command when a message containing a command is sent" in {
    val mockCommand = mock[Command]
    val mockFakeCommand = mock[Command]
    val mockMessage = mock[Message]
    implicit val mockDefaultCommand = mock[Command]
    implicit val commands = List(mockCommand, mockFakeCommand)

    mockTriggerMessage(mockCommand, "test")
    mockTriggerMessage(mockFakeCommand, "notWanted")

    when(mockMessage.text).thenReturn("""!test --with="some""")
    MessageHandler.handle(mockMessage)

    verify(mockCommand, times(1))
      .execute(any[ParsedMessage], any[Message])
    verify(mockDefaultCommand, times(0))
      .execute(any[ParsedMessage], any[Message])
  }

  it should "call the default handler if no command is found"  in {
    val mockCommand = mock[Command]
    val mockFakeCommand = mock[Command]
    implicit val mockDefaultCommand = mock[Command]
    val mockMessage = mock[Message]

    implicit val commands = List(mockCommand, mockFakeCommand)

    mockTriggerMessage(mockDefaultCommand, "default")
    mockTriggerMessage(mockCommand, "test")
    mockTriggerMessage(mockFakeCommand, "notWanted")

    when(mockMessage.text).thenReturn("""!commandThatDoesntExist --with="some""")
    MessageHandler.handle(mockMessage)

    commands.map(verifyCommandNotExecuted)
    verify(mockDefaultCommand, times(1))
      .execute(any[ParsedMessage], any[Message])
  }
}
