FROM hseeberger/scala-sbt as Builder

RUN mkdir -p /opt/build /opt/jar
COPY . /opt/build
WORKDIR /opt/build

RUN sbt assembly

FROM openjdk as Runner
COPY --from=Builder /opt/build/target/scala-*.*/slackBot.jar /opt/application/slackBot.jar

WORKDIR /opt/application
ENTRYPOINT java -jar slackBot.jar